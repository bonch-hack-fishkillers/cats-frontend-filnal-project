const state = {
  catPos: 0,
  phases: 0,
  phase: 'sad',
  redirect: false
}
const getters = {
  getPos (state) {
    return state.catPos
  },
  getPhase (state) {
    return state.phase
  },
  rdrct (state) {
    return state.redirect
  }
}
const actions = {
  setRandomPos (context) {
    context.commit('SET_RANDOM_POS')
  },
  togglePhase (context) {
    context.commit('TOGGLE_PHASE')
  },
  setPhases (context) {
    context.commit('SET_PHASES')
  }
}
const mutations = {
  SET_RANDOM_POS (state) {
    state.catPos = Math.floor(Math.random() * 3)
  },
  TOGGLE_PHASE (state) {
    Math.random() >= 0.5 ? state.phase = 'sad' : state.phase = 'happy'
    state.phases--
    if (state.phases === 0) {
      state.redirect = true
    }
  },
  SET_PHASES (state) {
    state.phases = Math.floor(Math.random() * (6 - 1) + 2)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
