import cats from './cats'
import floatingCat from './floatingCat'

export default {
  namespaced: true,
  modules: {
    cats, floatingCat
  }
}
