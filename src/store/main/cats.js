
const state = {
  current: 0,
  cats: [
    { id: 0, name: 'Кеша', image: '../statics/cats/cat1.jpg', description: 'Котик, который обожает обнимашки. Тискайте его столько,сколько хотите. Молодой и игривый малыш.', link: 'http://poteryashka.spb.ru/', tel: '+7 (812) 338-80-08', address: 'г. Санкт-Петербург,  ул Союза Печатников дом 24, лит Б, помещение 009' },
    { id: 1, name: 'Марти', image: '../statics/cats/cat2.jpg', description: 'Деловой кот. Всё что можно сказать о нем - "ты можешь почесать меня, но не переборщи"', link: 'http://domdog.ru/', tel: '+7 (931) 257-40-30', address: 'Санкт-Петербург, Стачек пр-кт., д.95, корп.1, кв. 280' },
    { id: 2, name: 'Дуняша', image: '../statics/cats/cat3.jpg', description: 'Маленькая кошечка, но уже с приключениями. Любит бегать везде, где только можно.', link: 'http://poteryashka.spb.ru/', tel: '+7 (812) 338-80-08+7 (812) 338-80-08', address: 'г. Санкт-Петербург,  ул Союза Печатников дом 24, лит Б, помещение 009' },
    { id: 3, name: 'Харви', image: '../statics/cats/cat4.jpg', description: 'Интроверт, не очень то и любит ваши обнимашки и целовашки. Больше предпочитает посидеть рядом с вами.', link: 'http://poteryashka.spb.ru/', tel: '+7 (931) 257-40-30', address: 'г. Санкт-Петербург,  ул Союза Печатников дом 24, лит Б, помещение 009' },
    { id: 4, name: 'Джесси', image: '../statics/cats/cat5.jpg', description: 'Пугливая кошечка. Но это на первый взгляд. Вскоре она подарит вам много любви и тепла.', link: 'http://priut-ostrovok.ru/', tel: '+7 (921) 380-02-69', address: 'г. Санкт-Петербург,  пр. Большевиков дом 22к8.' },
    { id: 5, name: 'Абсолем', image: '../statics/cats/cat6.jpg', description: 'Деловитый кот. Составит вам компанию за ужином.', link: 'http://priut-ostrovok.ru/', tel: '+7 (921) 380-02-69', address: 'г. Санкт-Петербург,  пр. Большевиков дом 22к8.' },
    { id: 6, name: 'Вызима', image: '../statics/cats/cat7.jpg', description: 'Любохгательная особа. Любит следить за хозяином и ходить за ним по всей квартире.', link: 'http://priut-ostrovok.ru/', tel: '+7 (921) 380-02-69', address: 'г. Санкт-Петербург,  пр. Большевиков дом 22к8.' },
    { id: 7, name: 'Адриана', image: '../statics/cats/cat8.jpg', description: 'Красивая кошка с повадками царицы. Очень разборчива в еде. Да и в людях тоже.', link: 'http://domdog.ru/', tel: '+7 (931) 257-40-30', address: 'Санкт-Петербург, Стачек пр-кт., д.95, корп.1, кв. 280' },
    { id: 8, name: 'Шура', image: '../statics/cats/cat9.jpg', description: 'Никаких поцелуйчиков для этой кошки. Она самостоятельная и независимая особа.', link: 'http://poteryashka.spb.ru/', tel: '+7 (812) 338-80-08', address: 'г. Санкт-Петербург,  ул Союза Печатников дом 24, лит Б, помещение 009' },
    { id: 10, name: 'Финик', image: '../statics/cats/cat10.jpg', description: 'Интересный кот с замечательными чертами характера. Ведь он будет любить вас всей своей душой.', link: 'http://domdog.ru/', tel: '+7 (931) 257-40-30', address: 'Санкт-Петербург, Стачек пр-кт., д.95, корп.1, кв. 280' }
  ]
}
const getters = {
  getCats (state) {
    return state.cats
  },
  getCurrent (state) {
    return state.cats[state.current]
  }
}
const actions = {
  changeCurrent (context, payload) {
    context.commit('CHANGE_CURRENT', payload)
  }
}
const mutations = {
  CHANGE_CURRENT (state, payload) {
    state.current = payload
  },
  GET_QUESTIONS (state, data) {
    state.questions = data
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
